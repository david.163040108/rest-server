<?php

class Mahasiswa_model extends CI_Model
{
	public function getAllMahasiswa()
	{
		return $this->db->get('mahasiswa')->result_array();
	}
	public function getMahasiswaById($id)
	{
		return $this->db->get_where('mahasiswa', ['id' => $id])->row_array();
	}

	public function insert($data){
    	$this->db->insert('mahasiswa', $data);
			return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
			$this->db->update($table, $data, array($par => $var));
			return $this->db->affected_rows();
    }

    public function delete($table, $par, $var){
			$this->db->where($par, $var);
			$this->db->delete($table);
			return $this->db->affected_rows();
    }
}