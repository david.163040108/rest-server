<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Mahasiswa extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mahasiswa_model');
	}
	
	public function index_get()
	{
		$id = $this->get('id');
		if ($id === null) {
			$mahasiswa = $this->Mahasiswa_model->getAllMahasiswa();
		} else {
			$mahasiswa = $this->Mahasiswa_model->getMahasiswaById($id);
		}
		if (!empty($mahasiswa)) {
			$this->set_response($mahasiswa, REST_Controller::HTTP_OK);
		} else {
			$this->set_response([
				'status' => FALSE,
				'massage' => 'User could not be found'], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	public function index_put()
    {
            $nrp = $this->put('nrp');
            $nama = $this->put('nama');
            $email = $this->put('email');
            $jurusan = $this->put('jurusan');
            $id = $this->put('id');
            $data = array(
                'nrp' => $nrp,
                'nama' => $nama,
                'email' => $email,
                'jurusan' => $jurusan
            );
            $update = $this->Mahasiswa_model->update('mahasiswa', $data, 'id', $id);

            if ($update) {
                $this->response(array('status' => 'succes', 200));
            } else {
                $this->response(array('status' => 'fail', 502));
            }
    }

    public function index_post()
    {
 			$nrp = $this->post('nrp');
            $nama = $this->post('nama');
            $email = $this->post('email');
            $jurusan = $this->post('jurusan');
            $data = array(
                'nrp' => $nrp,
                'nama' => $nama,
                'email' => $email,
                'jurusan' => $jurusan
            );
        $insert = $this->Mahasiswa_model->insert($data);

        if ($insert) {
            $this->response(array('status' => 'succes', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
      $id = $this->delete('id');
        $delete = $this->Mahasiswa_model->delete('mahasiswa', 'id', $id);

        if ($delete) {
            $this->response(array('status' => 'succes', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}